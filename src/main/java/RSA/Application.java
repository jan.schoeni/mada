package RSA;

public class Application {

    public static void main(String[] args) {
        //verschlüsseln
        Encryptor.saveKeys();
        Encryptor.saveChiffre(Encryptor.encrypt(Encryptor.readText()));

        //entschlüsseln
        Decryptor.decrypt(Decryptor.readChiffre());
    }

}
