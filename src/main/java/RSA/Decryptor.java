package RSA;

import java.io.BufferedWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;

public class Decryptor {
    private static final Path SK_FILE = Path.of("src/main/java/RSA/docs/sk.txt");
    private static final Path CHIFFRE_FILE = Path.of("src/main/java/RSA/docs/chiffre.txt");
    private static final Path TEXT_D_FILE = Path.of("src/main/java/RSA/docs/text-d.txt");
    private static BigInteger n = new BigInteger(readKey(SK_FILE)[0]);
    private static BigInteger d = new BigInteger(readKey(SK_FILE)[1]);

    public static String[] readKey(Path path) {
        String importedKey = "";
        try {
            importedKey = Files.readString(path);
        } catch (
                IOException ex) {
            System.err.println("Fehler beim Lesen");
        }

        // Den String bei jedem Komma teilen
        String[] parts = importedKey.substring(1,importedKey.length()-1).split(",");
        return parts;
    }

    public static String[] readChiffre() {
        //chiffreDatei einlesen
        String importedChiffre = "";
        try {
            importedChiffre = Files.readString(CHIFFRE_FILE);
        } catch (
                IOException ex) {
            System.err.println("Fehler beim Lesen");
        }

        // Den String bei jedem Komma teilen
        String[] parts = importedChiffre.split(",");
        return parts;
    }

    public static void decrypt(String[] letters) {
        //entschlüsseln: y^d mod(n)
        //schnelle Exponentiation
        String fullMessage = "";
        for (String letter : letters) {
            BigInteger decrypted = Math.fastExp(new BigInteger(letter), Math.toBinary(d), n);
            char character = (char) decrypted.intValue();
            fullMessage += character;
        }
        System.out.println("\nDie entschlüsselte Nachricht ist:\n" + fullMessage);
        try (BufferedWriter writer = Files.newBufferedWriter(TEXT_D_FILE)) {
            writer.write(fullMessage);
        } catch (IOException ex) {
            System.err.println("Fehler beim Schreiben");
        }
    }
}
