package RSA;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

public class Encryptor {
    private static final Path SK_FILE = Path.of("src/main/java/RSA/docs/sk.txt");
    private static final Path PK_FILE = Path.of("src/main/java/RSA/docs/pk.txt");
    private static final Path TEXT_FILE = Path.of("src/main/java/RSA/docs/text.txt");
    private static final Path CHIFFRE_FILE = Path.of("src/main/java/RSA/docs/chiffre.txt");

    private static BigInteger p;
    private static BigInteger q;
    private static BigInteger n;
    private static BigInteger e;
    private static BigInteger d;

    public static void create_n() {
        SecureRandom random = new SecureRandom();
        int bitLength = 1024; // Beispiel für eine 1024-Bit-Zahl
        p = BigInteger.probablePrime(bitLength, random);

        do {
            q = BigInteger.probablePrime(bitLength, random);
        } while (q.equals(p)); // Wiederhole, bis q != p

        n = p.multiply(q);
    }

    public static void create_ed() {
        // φ(n) berechnen
        BigInteger y = p.subtract(BigInteger.ONE).multiply(q.subtract(BigInteger.ONE));

        e = BigInteger.TWO;
        // e wählen das in Z*y(n)
        // also in {0,1,...,n-1} und teilerfremd zu y(n)
        while (!Math.extEuclidAlg(y, e)[0].equals(BigInteger.ONE)) {
            e = e.add(BigInteger.ONE);
        }

        // d wählen das in Z*y(n)
        // also in {0,1,...,n-1} und teilerfremd zu y(n)
        // ausserdem muss e*d mod(y(n)) kongruent 1 sein. automatisch der Fall, wenn e in Z*
        d = Math.extEuclidAlg(y, e)[2];
        if (d.compareTo(BigInteger.ZERO) < 0) {
            d = d.add(y);
        }
    }

    public static void saveKeys() {
        create_n();
        create_ed();

        //privater Schlüssel speichern
        try (BufferedWriter writer = Files.newBufferedWriter(SK_FILE)) {
            writer.write("(" + n + "," + d + ")");
        } catch (
                IOException ex) {
            System.err.println("Fehler beim Schreiben");
        }

        //öffentlicher Schlüssel speichern
        try (BufferedWriter writer = Files.newBufferedWriter(PK_FILE)) {
            writer.write("(" + n + "," + e + ")");
        } catch (
                IOException ex) {
            System.err.println("Fehler beim Schreiben");
        }
    }

    public static List<Integer> readText() {
        //input einlesen
        List<Integer> asciiCodes = new ArrayList<>();
        try (
                BufferedReader reader = Files.newBufferedReader(TEXT_FILE)) {
            int character;
            while ((character = reader.read()) != -1) {
                asciiCodes.add(character);
            }
        } catch (
                IOException ex) {
            System.err.println("Fehler beim Lesen");
        }
        return asciiCodes;
    }

    public static String encrypt(List<Integer> asciiCodes) {
        //verschlüsseln: x^e mod(n)
        //schnelle Exponentiation
        String fullChiffre = "";
        for (int code : asciiCodes) {
            if (BigInteger.valueOf(code).compareTo(n) >= 0) {
                System.err.println("x ist zu gross für dieses n");
            }

            BigInteger chiffre = Math.fastExp(BigInteger.valueOf(code), Math.toBinary(e), n);
            fullChiffre += chiffre + ",";
        }

        fullChiffre = fullChiffre.substring(0, fullChiffre.length() - 1);
        return fullChiffre;
    }

    public static void saveChiffre(String fullChiffre) {
        try (BufferedWriter writer = Files.newBufferedWriter(CHIFFRE_FILE)) {
            writer.write(fullChiffre);
        } catch (IOException ex) {
            System.err.println("Fehler beim Schreiben");
        }
    }
}
