package Huffmann;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class HuffmanDecryptor {
    private static final String OUTPUT = "src/main/java/Huffmann/docs/output-mada.dat";
    private static final Path DEC_TAB = Path.of("src/main/java/Huffmann/docs/dec_tab-mada.txt");
    private static final Path DECOMPRESS = Path.of("src/main/java/Huffmann/docs/decompress.txt");

    public static byte[] importByteArray() {
        try {
            File file = new File(OUTPUT);
            byte[] bFile = new byte[(int) file.length()];
            FileInputStream fis = new FileInputStream(file);
            fis.read(bFile);
            fis.close();
            System.out.println("Byte-Array in " + OUTPUT + " wurde gelesen.");
            return bFile;
        } catch (IOException e) {
            System.out.println(e);
            return null;
        }
    }

    public static Map<Character, String> readTable() {
        String fullString = "";
        try (BufferedReader reader = Files.newBufferedReader(DEC_TAB)) {
            fullString = reader.readLine();
            System.out.println("Huffman-Tabelle in " + DEC_TAB + " wurde gelesen");
        } catch (IOException e) {
            System.out.println("Fehler beim Lesen der Datei: " + e);
        }

        Map<Character, String> huffmanCodes = new HashMap<>();
        String[] parts = fullString.split("-");
        for (String part : parts) {
            char character = (char) Integer.parseInt(part.split(":")[0]);
            String code = part.split(":")[1];
            huffmanCodes.put(character, code);
        }

        return huffmanCodes;
    }

    public static String readByteArray(byte[] input) {
        String bitString = "";
        for (byte b : input) {
            //konvertiert das Byte in einen positiven Integer-Wert im Bereich von 0 bis 255.
            Integer value = b & 0xFF;

            //konvertiert den Integer-Wert in einen Binärstring.
            String binaryValue = Integer.toBinaryString(value);

            //stellt sicher, dass der Binärstring 8 Zeichen lang ist.
            binaryValue = String.format("%8s", binaryValue);

            //ersetzt führende Leerzeichen mit Nullen, um sicherzustellen, dass der Binärstring genau 8 Bits enthält.
            binaryValue = binaryValue.replace(' ', '0');

            //fügt alle Strings zusammen
            bitString += binaryValue;
        }
        return bitString;
    }

    public static String decode(String bitString, Map<Character, String> huffmanCodes) {
        //"Füller" entfernen
        int lastIndex = bitString.lastIndexOf('1'); //gibt den Index der letzten Vorkommen von '1' im String zurück. Wenn keine '1' gefunden wird, gibt die Methode -1 zurück.
        bitString = bitString.substring(0, lastIndex);

        System.out.println("\nDer Code lautet:\n"+bitString);

        String clearText = "";
        String part = "";
        for (int i = 0; i < bitString.length(); i++) {
            part += bitString.charAt(i);
            if (huffmanCodes.containsValue(part)) {
                clearText += getKeyByValue(huffmanCodes, part);
                part = "";
            }
        }
        System.out.println("\nDer Klartext lautet:\n"+clearText);
        return clearText;
    }

    public static void saveText(String clearText) {
        try (BufferedWriter writer = Files.newBufferedWriter(DECOMPRESS)) {
            writer.write(clearText);
            System.out.println("\nDer Klartext wurde in "+DECOMPRESS+" gespeichert");
        } catch (IOException e) {
            System.out.println("Fehler beim Schreiben der Datei: " + e);
        }
    }

    public static Character getKeyByValue(Map<Character, String> map, String value) {
        for (Map.Entry<Character, String> entry : map.entrySet()) {
            if (entry.getValue().equals(value)) {
                return entry.getKey();
            }
        }
        return null; // Falls kein Schlüssel für den Wert gefunden wird
    }
}
