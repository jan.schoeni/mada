package Huffmann;

import java.util.HashMap;
import java.util.Map;

import static Huffmann.HuffmanDecryptor.*;
import static Huffmann.HuffmanEncryptor.*;

public class Application {
    public static Map<Character, String> huffmanCodes;

    public static void main(String[] args) {
        //// HuffmanTree erstellen
        //HuffmanNode root = buildHuffmanTree(countChars());
//
        //// Huffman-Codes extrahieren
        //huffmanCodes = new HashMap<>();
        //generateCodes(root, "", huffmanCodes);
//
        //// Huffman-Codes speichern
        //saveTable(huffmanCodes);
//
        //// Text übersetzen
        //saveByteArray(createByteArray(createCode(huffmanCodes)));

        ///----///

        huffmanCodes = readTable();
        saveText(decode(readByteArray(importByteArray()), huffmanCodes));
    }
}
