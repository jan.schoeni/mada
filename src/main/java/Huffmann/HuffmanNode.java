package Huffmann;

public class HuffmanNode implements Comparable<HuffmanNode>{
        int frequency;
        char character;
        HuffmanNode left;
        HuffmanNode right;

        HuffmanNode(char character, int frequency) {
            this.character = character;
            this.frequency = frequency;
        }

        HuffmanNode(int frequency) {
            this.character = '\0';
            this.frequency = frequency;
        }

        @Override
        public int compareTo(HuffmanNode o) {
            return Integer.compare(this.frequency, o.frequency);
        }
}
