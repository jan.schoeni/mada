package Huffmann;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public class HuffmanEncryptor {
    private static final Path TEXT_FILE = Path.of("src/main/java/Huffmann/docs/text.txt");
    private static final Path DEC_TAB = Path.of("src/main/java/Huffmann/docs/dec_tab-mada.txt");
    private static final String OUTPUT = "src/main/java/Huffmann/docs/output.dat";

    public static int[] countChars() {
        // Array zum Zählen der Zeichenhäufigkeit
        int[] asciiCount = new int[128];

        // Datei einlesen und Zeichen zählen
        try (BufferedReader reader = Files.newBufferedReader(TEXT_FILE)) {
            int character;
            while ((character = reader.read()) != -1) {
                if (character < 128) { // Nur ASCII-Zeichen zählen
                    asciiCount[character]++;
                }
            }
        } catch (IOException e) {
            System.out.println("Fehler beim Einlesen der Datei: " + e);
        }
        return asciiCount;
    }

    public static HuffmanNode buildHuffmanTree(int[] charFreqs) {
        List<HuffmanNode> nodeList = new ArrayList<>();

        // Alle Zeichen mit einer Häufigkeit größer als 0 in die Liste einfügen
        for (int i = 0; i < charFreqs.length; i++) {
            if (charFreqs[i] > 0) {
                nodeList.add(new HuffmanNode((char) i, charFreqs[i]));
            }
        }

        // Knoten kombinieren, bis nur noch einer übrig ist
        while (nodeList.size() > 1) {
            // Liste sortieren
            Collections.sort(nodeList);

            // Die zwei Knoten mit den kleinsten Häufigkeiten entfernen
            HuffmanNode left = nodeList.remove(0);
            HuffmanNode right = nodeList.remove(0);

            // Neuer Knoten mit kombinierten Häufigkeiten
            HuffmanNode parent = new HuffmanNode(left.frequency + right.frequency);
            parent.left = left;
            parent.right = right;

            // Neuen Knoten wieder in die Liste einfügen
            nodeList.add(parent);
        }
        return nodeList.get(0);
    }

    public static void generateCodes(HuffmanNode node, String code, Map<Character, String> codeMap) {
        if (node != null) {
            if (node.left == null && node.right == null) { // wenn es ein Blatt ist
                codeMap.put(node.character, code);
            }
            generateCodes(node.left, code + '0', codeMap);
            generateCodes(node.right, code + '1', codeMap);
        }
    }

    public static void saveTable(Map<Character, String> huffmanCodes) {
        String fullString = "";
        for (Map.Entry<Character, String> entry : huffmanCodes.entrySet()) {
            //ASCII-Code von Zeichen1:Code von Zeichen1-ASCII-Code von Zeichen2:Code von Zeichen2-
            fullString += (int) entry.getKey() + ":" + entry.getValue() + "-";
        }
        try (BufferedWriter writer = Files.newBufferedWriter(DEC_TAB)) {
            writer.write(fullString);
            System.out.println("Huffman-Tabelle wurde in "+DEC_TAB+" gespeichert");
        } catch (IOException e) {
            System.out.println("Fehler beim Schreiben der Datei: " + e);
        }
    }

    public static String createCode(Map<Character, String> huffmanCodes) {

        // Datei einlesen und in Code umwandeln
        String bitString = "";
        try (BufferedReader reader = Files.newBufferedReader(TEXT_FILE)) {
            int character;
            while ((character = reader.read()) != -1) {
                bitString += huffmanCodes.get((char) character);
            }
        } catch (IOException e) {
            System.out.println("Fehler beim Einlesen der Datei: " + e);
        }

        //An diesen Bitstring soll eine 1 und anschliessend so viele Nullen dran gehängt werden, bis
        //der Bitstring eine Länge hat, die ein Vielfaches von 8 ist.
        bitString += "1";
        while (bitString.length() % 8 != 0) {
            bitString += "0";
        }
        return bitString;
    }

    public static byte[] createByteArray(String bitString) {
        byte[] byteArray = new byte[bitString.length() / 8];
        for (int i = 0; i < byteArray.length; i++) {
            String byteString = bitString.substring(8 * i, 8 * i + 8);
            byteArray[i] = (byte) Integer.parseInt(byteString, 2);
        }
        return byteArray;
    }

    public static void saveByteArray(byte[] out) {
        try {
            FileOutputStream fos = new FileOutputStream(OUTPUT);
            fos.write(out);
            fos.close();
            System.out.println("Byte-Array wurde in "+OUTPUT+" geschrieben.");
        } catch (IOException e) {
            System.out.println(e);
        }
    }
}
