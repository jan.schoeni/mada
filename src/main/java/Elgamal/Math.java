package Elgamal;

import java.math.BigInteger;

public class Math {
    public static BigInteger[] extEuclidAlg(BigInteger a, BigInteger b) {
        BigInteger x0 = BigInteger.ONE;
        BigInteger y0 = BigInteger.ZERO;
        BigInteger x1 = BigInteger.ZERO;
        BigInteger y1 = BigInteger.ONE;
        while (!b.equals(BigInteger.ZERO)) {
            BigInteger q = a.divide(b);
            BigInteger r = a.mod(b);
            a = b;
            b = r;
            BigInteger tempX0 = x0;
            BigInteger tempY0 = y0;
            BigInteger tempX1 = x1;
            BigInteger tempY1 = y1;
            x0 = x1;
            y0 = y1;
            x1 = tempX0.subtract(q.multiply(tempX1));
            y1 = tempY0.subtract(q.multiply(tempY1));
        }
        BigInteger[] result = {a, x0, y0};
        return result;
    }

    public static BigInteger fastExp(BigInteger x, String e, BigInteger n) {
        BigInteger i = BigInteger.valueOf(e.length() - 1);
        BigInteger h = BigInteger.ONE;
        BigInteger k = x;

        while (i.compareTo(BigInteger.ZERO) >= 0) {
            if (e.charAt(i.intValue()) == '1') {
                h = h.multiply(k).mod(n);
            }
            k = k.pow(2).mod(n);
            i = i.subtract(BigInteger.ONE);
        }
        return h;
    }

    public static String toBinary(BigInteger decimal) {
        String binaryNumber = "";
        BigInteger remainder;

        do {
            remainder = decimal.mod(BigInteger.TWO);
            decimal = decimal.divide(BigInteger.TWO);

            binaryNumber = remainder + binaryNumber;
        } while (!decimal.equals(BigInteger.ZERO));

        return binaryNumber;
    }
}
