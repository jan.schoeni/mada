package Elgamal;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Elgamal {
    private static final Path SK_FILE = Path.of("src/main/java/Elgamal/docs/sk.txt");
    private static final Path PK_FILE = Path.of("src/main/java/Elgamal/docs/pk.txt");
    private static final Path CHIFFRE_FILE = Path.of("src/main/java/Elgamal/docs/chiffre.txt");
    private static final Path TEXT_FILE = Path.of("src/main/java/Elgamal/docs/text.txt");
    private static final Path TEXT_D_FILE = Path.of("src/main/java/Elgamal/docs/text-d.txt");

    public static BigInteger n = new BigInteger(
            "FFFFFFFFFFFFFFFFC90FDAA22168C234C4C6628B80DC1CD1" +
                    "29024E088A67CC74020BBEA63B139B22514A08798E3404DD" +
                    "EF9519B3CD3A431B302B0A6DF25F14374FE1356D6D51C245" +
                    "E485B576625E7EC6F44C42E9A637ED6B0BFF5CB6F406B7ED" +
                    "EE386BFB5A899FA5AE9F24117C4B1FE649286651ECE45B3D" +
                    "C2007CB8A163BF0598DA48361C55D39A69163FA8FD24CF5F" +
                    "83655D23DCA3AD961C62F356208552BB9ED529077096966D" +
                    "670C354E4ABC9804F1746C08CA18217C32905E462E36CE3B" +
                    "E39E772C180E86039B2783A2EC07A28FB5C55DF06F4C52C9" +
                    "DE2BCBF6955817183995497CEA956AE515D2261898FA0510" +
                    "15728E5A8AACAA68FFFFFFFFFFFFFFFF", 16);

    // Erzeuger der zyklischen Gruppe
    public static BigInteger g = BigInteger.TWO;

    public static BigInteger b;
    public static BigInteger g_pow_b;

    public static void main(String[] args) {
        //// ausführen mit eigenem Text und Schlüssel
        //generatePrivateKey();
        //generatePublicKey(b);
        //encrypt(readText());
        //decrypt();

        // ausführen mit erhaltenem Text und (privatem) Schlüssel
        b = readKey(SK_FILE);
        generatePublicKey(b);
        decrypt();
    }

    //generiert nur den letzten Teil
    public static void generatePrivateKey() {
        //Zufälliges b zwischen 1 und ord(G)-1 wählen
        BigInteger ordnungG = n.subtract(BigInteger.ONE);
        Random random = new Random();
        do {
            b = new BigInteger(n.bitLength(), random);
        } while (b.compareTo(BigInteger.ONE) <= 0 || b.compareTo(ordnungG.subtract(BigInteger.ONE)) >= 0);

        //privaten Schlüssel abspeichern
        try (BufferedWriter writer = Files.newBufferedWriter(SK_FILE)) {
            writer.write(b.toString());
        } catch (
                IOException ex) {
            System.err.println("Fehler beim Schreiben des privaten Schlüssels");
        }
    }

    //generiert nur den letzten Teil
    public static void generatePublicKey(BigInteger b) {
        //g^b rechnen
        g_pow_b = g.modPow(b, n);

        //öffentlichen Schlüssel abspeichern
        try (BufferedWriter writer = Files.newBufferedWriter(PK_FILE)) {
            writer.write(g_pow_b.toString());
        } catch (
                IOException ex) {
            System.err.println("Fehler beim Schreiben des öffentlichen Schlüssels");
        }
    }

    public static List<Integer> readText() {
        //input einlesen
        List<Integer> asciiCodes = new ArrayList<>();
        try (
                BufferedReader reader = Files.newBufferedReader(TEXT_FILE)) {
            int character;
            while ((character = reader.read()) != -1) {
                asciiCodes.add(character);
            }
        } catch (
                IOException ex) {
            System.err.println("Fehler beim Lesen der Datei: " + TEXT_FILE);
        }
        return asciiCodes;
    }

    public static BigInteger readKey(Path path) {
        String importedKey = "";
        try {
            importedKey = Files.readString(path);
        } catch (
                IOException ex) {
            System.err.println("Fehler beim Lesen der Datei: " + TEXT_FILE);
        }

        return new BigInteger(importedKey);
    }

    public static void encrypt(List<Integer> asciiCodes) {
        //Zufälliges a zwischen 1 und ord(G)-1 wählen
        Random random = new Random();
        BigInteger a;
        do {
            a = new BigInteger(n.bitLength(), random);
        } while (a.compareTo(BigInteger.ONE) <= 0 || a.compareTo(n.subtract(BigInteger.ONE)) >= 0);
        //BigInteger a = BigInteger.TWO;


        //g^a rechnen
        BigInteger g_pow_a = g.modPow(a, n);

        //g^b^a rechnen
        BigInteger g_poq_b_pow_a = g_pow_b.modPow(a, n);

        String chiffres = "";
        for (Integer ascii : asciiCodes) {
            chiffres += "(" + g_pow_a + "," + g_poq_b_pow_a.multiply(BigInteger.valueOf(ascii)).mod(n) + ");";
        }

        //Schreiben in der Form (1221, 23323); (232332, 1122); . . .)
        //in die Datei chiffre.txt
        try (BufferedWriter writer = Files.newBufferedWriter(CHIFFRE_FILE)) {
            writer.write(chiffres);
        } catch (IOException ex) {
            System.err.println("Fehler beim Schreiben der Chiffres");
        }
    }

    public static void decrypt() {
        //chiffreDatei einlesen
        String importedChiffre = "";
        try {
            importedChiffre = Files.readString(CHIFFRE_FILE);
        } catch (
                IOException ex) {
            System.err.println("Fehler beim Lesen der Chiffres");
        }

        // Den String bei jedem Semikolon teilen
        String[] parts = importedChiffre.split(";");

        //entschlüsseln
        String fullMessage = "";
        for (String part : parts) {
            part = part.substring(1, part.length() - 1);
            String[] y = part.split(",");
            BigInteger asciiCode = new BigInteger(y[1]).multiply((new BigInteger(y[0])).modPow(b, n).modInverse(n)).mod(n);
            fullMessage += (char) asciiCode.intValue();
        }

        //Schreiben des entschlüsselten Texts
        try (BufferedWriter writer = Files.newBufferedWriter(TEXT_D_FILE)) {
            writer.write(fullMessage);
        } catch (IOException ex) {
            System.err.println("Fehler beim Schreiben der fullMessage: " + ex);
        }
        System.out.println("Die Nachricht ist: \n" + fullMessage);
    }
}